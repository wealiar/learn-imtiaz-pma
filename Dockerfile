# FROM ubuntu:latest
FROM ubuntu-jdk

LABEL maintainer="Tsybulin Denis \"tsybulindenis@gmail.com\"" 

# RUN apt-get update && apt-get install -y openjdk-8-jdk
RUN apt-get update

ENV version=aws-db-usage
ENV dbuser=postgres
ENV dbpass=password321
#ENV jdbcurl=jdbc:driver://hostname:port/dbname
ENV jdbcurl=jdbc:postgresql://pmadatabaseaws.ceveyzditlpz.us-east-2.rds.amazonaws.com:5432/postgres

WORKDIR /usr/local/bin

ADD target/pma-app.jar . 

ENTRYPOINT ["java", "-jar", "pma-app.jar"]