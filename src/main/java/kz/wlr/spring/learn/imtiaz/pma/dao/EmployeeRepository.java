package kz.wlr.spring.learn.imtiaz.pma.dao;

import kz.wlr.spring.learn.imtiaz.pma.dto.EmployeeProject;
import kz.wlr.spring.learn.imtiaz.pma.entities.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "restEmployees", path = "restEmployees")
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

    @Query(nativeQuery = true, value = "select e.first_name as firstName, e.last_name as lastName, COUNT(pe.employee_id) as projectCount " +
            "FROM employee e left join project_employee pe ON pe.employee_id = e.employee_id " +
            "GROUP BY e.first_name, e.last_name ORDER BY 3 DESC")
    public List<EmployeeProject> employeeProjects();

    public Employee findByEmail(String value);

    public Employee findByEmployeeId(long id);

    // https://docs.spring.io/spring-data/rest/docs/3.2.5.RELEASE/reference/html/#reference
    public List<Employee> findByFirstName(@Param("firstName") String firstName); // http://localhost:8080/restEmployees/search/findByFirstName?firstName=Jim
}
