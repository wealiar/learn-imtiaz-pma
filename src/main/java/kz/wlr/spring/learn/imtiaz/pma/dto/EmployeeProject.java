package kz.wlr.spring.learn.imtiaz.pma.dto;

public interface EmployeeProject {
    public String getFirstName();
    public String getLastName();
    public int getProjectCount();
}
