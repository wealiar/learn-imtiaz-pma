package kz.wlr.spring.learn.imtiaz.pma.services;

import kz.wlr.spring.learn.imtiaz.pma.dao.EmployeeRepository;
import kz.wlr.spring.learn.imtiaz.pma.dto.EmployeeProject;
import kz.wlr.spring.learn.imtiaz.pma.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository empRepo;

    public Employee save(Employee employee) {
        return empRepo.save(employee);
    }

    public List<Employee> getAll() {
        return (List<Employee>) empRepo.findAll();
    }

    public Iterable<Employee> findAll() {
        return empRepo.findAll();
    }

    public List<EmployeeProject> employeeProjects() {
        return empRepo.employeeProjects();
    }

    public Employee findById(long employeeId) {
        return empRepo.findById(employeeId).get();
    }

    public void deleteById(long id) {
        empRepo.deleteById(id);
    }

    public Employee findByEmail(String value) {
        return empRepo.findByEmail(value);
    }

    public Employee findByEmployeeId(long id) {
        return empRepo.findByEmployeeId(id);
    }

    public Iterable<Employee> findAll(Pageable pageAndSize) {
        return empRepo.findAll(pageAndSize);
    }
}
