package kz.wlr.spring.learn.imtiaz.pma.dao;

import kz.wlr.spring.learn.imtiaz.pma.entities.UserAccount;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountRepository extends PagingAndSortingRepository<UserAccount, Long> {
    @Override
    List<UserAccount> findAll();
}
