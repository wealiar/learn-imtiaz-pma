package kz.wlr.spring.learn.imtiaz.pma.services;

import kz.wlr.spring.learn.imtiaz.pma.dao.ProjectRepository;
import kz.wlr.spring.learn.imtiaz.pma.dto.ChartData;
import kz.wlr.spring.learn.imtiaz.pma.entities.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {
    @Autowired
    ProjectRepository proRepo;

    public Project save(Project project) {
        return proRepo.save(project);
    }

    public List<Project> getAll() {
        return proRepo.findAll();
    }

    public List<ChartData> getProjectStatus() {
        return proRepo.getProjectStatus();
    }

    public Project findById(long projectId) {
        return proRepo.findById(projectId).get();
    }

    public void deleteById(long id) {
        proRepo.deleteById(id);
    }

    public Iterable<Project> findAll(Pageable pageAndSize) {
        return proRepo.findAll(pageAndSize);
    }
}
