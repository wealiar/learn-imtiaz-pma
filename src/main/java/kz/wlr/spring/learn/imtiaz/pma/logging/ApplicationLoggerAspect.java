package kz.wlr.spring.learn.imtiaz.pma.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class ApplicationLoggerAspect {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Pointcut("within(kz.wlr.spring.learn.imtiaz.pma.controllers..*) " +
            "|| within(kz.wlr.spring.learn.imtiaz.pma.api.controllers..*)")
    public void definePackagePointcuts() {
        // empty method just to name the location specified in the pointcut
    }

//    @Before("definePackagePointcuts()")
//    public void logBefore(JoinPoint jp){
//        log.debug("\n\n\n ");
//        log.debug("********** Before Method Execution ********** \n {}. {} with arguments [s] = {}",
//                jp.getSignature().getDeclaringTypeName(),
//                jp.getSignature().getName(),
//                Arrays.toString(jp.getArgs()));
//        log.debug(" _________________________________________________\n\n\n");
//    }
//
//    @After("definePackagePointcuts()")
//    public void logAfter(JoinPoint jp){
//        log.debug("\n\n\n ");
//        log.debug("********** After Method Execution ********** \n {}. {} with arguments [s] = {}",
//                jp.getSignature().getDeclaringTypeName(),
//                jp.getSignature().getName(),
//                Arrays.toString(jp.getArgs()));
//        log.debug(" _________________________________________________\n\n\n");
//    }

    @Around("definePackagePointcuts()")
    public Object logAround(ProceedingJoinPoint pjp){
        log.debug("\n\n\n ");
        log.debug("********** Before Method Execution ********** \n {}. {} with arguments [s] = {}",
                pjp.getSignature().getDeclaringTypeName(),
                pjp.getSignature().getName(),
                Arrays.toString(pjp.getArgs()));
        log.debug(" _________________________________________________\n\n\n");

        Object o = null;
        try {
            o = pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        log.debug("\n\n\n ");
        log.debug("********** After Method Execution ********** \n {}. {} with arguments [s] = {}",
                pjp.getSignature().getDeclaringTypeName(),
                pjp.getSignature().getName(),
                Arrays.toString(pjp.getArgs()));
        log.debug(" _________________________________________________\n\n\n");

        return o;
    }


}
