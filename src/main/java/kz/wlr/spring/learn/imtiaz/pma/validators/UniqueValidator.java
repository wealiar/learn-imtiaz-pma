package kz.wlr.spring.learn.imtiaz.pma.validators;

import kz.wlr.spring.learn.imtiaz.pma.entities.Employee;
import kz.wlr.spring.learn.imtiaz.pma.services.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class UniqueValidator implements ConstraintValidator<UniqueValue, String> {
    @Autowired
    EmployeeService employeeService;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        System.out.println("Entered validation method");
        log.info("Entered validation method");

        Employee employee = employeeService.findByEmail(value);
        return (employee == null);
    }
}
