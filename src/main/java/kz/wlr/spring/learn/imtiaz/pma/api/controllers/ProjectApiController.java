package kz.wlr.spring.learn.imtiaz.pma.api.controllers;

import kz.wlr.spring.learn.imtiaz.pma.entities.Project;
import kz.wlr.spring.learn.imtiaz.pma.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/app-api/projects")
public class ProjectApiController {
    @Autowired
    ProjectService projectService;

    @GetMapping
    public Iterable<Project> getProjects() {
        return projectService.getAll();
    }

    @GetMapping("/{id}")
    public Project getProjectById(@PathVariable("id") long id) {
        return projectService.findById(id);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Project create(@RequestBody Project project) {
        return projectService.save(project);
    }

    @PutMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Project update(@RequestBody Project project) {
        return projectService.save(project);
    }

    @PatchMapping(path = "/{id}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Project partialUpdate(@PathVariable("id") long id, @RequestBody Project patchProject) {
        Project project = projectService.findById(id);

        if (patchProject.getDescription() != null)
            project.setDescription(patchProject.getDescription());
        if (patchProject.getName() != null)
            project.setName(patchProject.getName());
        if (patchProject.getStage() != null)
            project.setStage(patchProject.getStage());

        return projectService.save(project);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") long id) {
        try {
            projectService.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
        }
    }

    @GetMapping(params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Project> findPaginatedProjects(@RequestParam("page") int page,
                                                    @RequestParam("size") int size) {
        Pageable pageAndSize = PageRequest.of(page, size);
        return projectService.findAll(pageAndSize);
    }
}
