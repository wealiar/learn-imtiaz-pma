package kz.wlr.spring.learn.imtiaz.pma.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_accounts")
public class UserAccount {
    @Id
    @Column(name = "user_id")
    @SequenceGenerator(name = "user_accounts_seq_name", sequenceName = "user_accounts_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_accounts_seq_name")
    private long userId;

    @Column(name = "username") // just to show that we can customize it
    private String userName;

    private String email;
    private String password;
    private boolean enabled = true;

    public UserAccount() {}

    public UserAccount(String userName, String email, String password, boolean enabled) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.enabled = enabled;
    }
}
