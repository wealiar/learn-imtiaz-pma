package kz.wlr.spring.learn.imtiaz.pma.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kz.wlr.spring.learn.imtiaz.pma.dto.ChartData;
import kz.wlr.spring.learn.imtiaz.pma.dto.EmployeeProject;
import kz.wlr.spring.learn.imtiaz.pma.entities.Project;
import kz.wlr.spring.learn.imtiaz.pma.services.EmployeeService;
import kz.wlr.spring.learn.imtiaz.pma.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    @Value("${version}")
    private String ver;

    @Autowired
    ProjectService proService;

    @Autowired
    EmployeeService empService;

    @GetMapping("/")
    public String displayHome(Model model) throws JsonProcessingException {
        model.addAttribute("versionNumber", ver);

        List<Project> projects = proService.getAll();
        model.addAttribute("projectsList", projects);

        List<ChartData> projectData = proService.getProjectStatus();
        // convert projectData object into a json structure for use in JS
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(projectData);
        model.addAttribute("projectStatusCnt", jsonString);

        List<EmployeeProject> employeesProjectCnt = empService.employeeProjects();
        model.addAttribute("employeesListProjectCnt", employeesProjectCnt);

        return "main/home";
    }
}
