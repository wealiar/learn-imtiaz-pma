package kz.wlr.spring.learn.imtiaz.pma.dao;

import kz.wlr.spring.learn.imtiaz.pma.dto.ChartData;
import kz.wlr.spring.learn.imtiaz.pma.entities.Project;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "restProjects", path = "restProjects")
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {
    @Override
    public List<Project> findAll();

    @Query(nativeQuery = true, value = "SELECT stage as label, COUNT(project_id) as value " +
            "FROM project " +
            "GROUP BY stage")
    public List<ChartData> getProjectStatus();

    // https://docs.spring.io/spring-data/rest/docs/3.2.5.RELEASE/reference/html/#reference
    public List<Project> findByName(@Param("name") String name); // http://localhost:8080/restProjects/search/findByName?name=Test%20project
}
