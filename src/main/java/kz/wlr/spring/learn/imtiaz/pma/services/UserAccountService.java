package kz.wlr.spring.learn.imtiaz.pma.services;

import kz.wlr.spring.learn.imtiaz.pma.dao.UserAccountRepository;
import kz.wlr.spring.learn.imtiaz.pma.entities.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAccountService {
    @Autowired
    UserAccountRepository userAccountRepository;

    public UserAccount save(UserAccount userAccount) {
        return userAccountRepository.save(userAccount);
    }

    public List<UserAccount> getAll() {
        return userAccountRepository.findAll();
    }
}
