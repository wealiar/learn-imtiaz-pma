package kz.wlr.spring.learn.imtiaz.pma.dto;

public interface ChartData {
    public String getLabel();
    public long getValue();
}
